package org.kav.jay.hazelcast.client;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@SpringBootTest
class HazelcastClientApplicationTests {

	@Test
	void contextLoads() {
		Parent a = new ChildA();
		Parent b = new ChildB();
		Parent c = new ChildC();

		List<Class> supportedTypes = Arrays.asList(ChildA.class, ChildB.class, ChildC.class);

		if (supportedTypes.contains(b.getClass())) {
			System.err.println("Found type!");
		}
	}

	@Test
	public void getHash(){
		String value1 = "006Smsisdn013S4915316589321_006Smsisdn013S4915308529668";
		String value2 = "006Smsisdn013S4915308529668_006Smsisdn013S4915316589321";
		System.err.println(Objects.hash());
		System.err.println(Objects.hash());
	}

}
