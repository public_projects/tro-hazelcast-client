package org.kav.jay.hazelcast.client.manager.action;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class HazelcastService {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private HazelcastInstance client;

    public HazelcastService(HazelcastInstance client){
        this.client = client;
    }

    public void clearMap(String mapName) {
        StringBuilder sb = new StringBuilder();
        Set<Object> keys = this.client.getMap(mapName).keySet();
        keys.forEach(o -> {
            sb.append(System.lineSeparator()).append("\t").append(mapName).append(" => ").append(o.toString());
        });
        this.client.getMap(mapName).clear();
        log.info("{}",sb);
        log.info("hz map [{}] cleared!", mapName);
    }

    public void clearAllMaps() {
        Collection<DistributedObject> instances = client.getDistributedObjects();
        int count = 1;
        for (DistributedObject instance : instances) {
            if (instance.getServiceName().equals("hz:impl:mapService")) {
                log.info("{}.[size: {}] map name: [{}]", count++, client.getMap(instance.getName()).size(), instance.getName());
                clearMap(instance.getName());
            }
        }
    }

    public void shutdown() {
        client.shutdown();
    }
}
