package org.kav.jay.hazelcast.client.manager;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceNotActiveException;
import org.kav.jay.hazelcast.client.manager.action.HazelcastService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;

@Service
public class ClientConnection implements CommandLineRunner {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private HazelcastInstance client;

    @Autowired
    private HazelcastService hazelcastService;

    @Override
    public void run(String... args) throws Exception {
//        hazelcastService.getMaps();
//        clearMap.clearMap("msisdn-identity-to-targets");
//        clearMap.clearMap("imsi-identity-to-targets");
//        clearMap.clearMap("imei-identity-to-targets");
//        clearMap.clearMap("document-identity-to-targets");
//        clearMap.clearMap("adsl-identity-to-targets");
//        clearMap.clearMap("mcng-identity-to-targets");
//        clearMap.clearMap("online-identity-to-targets");
//
//        clearMap.clearMap("target-to-msisdn-identity");
//        clearMap.clearMap("target-to-imsi-identity");
//        clearMap.clearMap("target-to-imei-identity");
//        clearMap.clearMap("target-to-document-identity");
//        clearMap.clearMap("target-to-adsl-identity");
//        clearMap.clearMap("target-to-mcng-identity");
//        clearMap.clearMap("target-to-online-identity");
//
//        // EBA
        hazelcastService.clearMap("ebaGraph");
        hazelcastService.clearMap("ebaGraphFiltered");
        hazelcastService.clearMap("ebaStatistics");
        hazelcastService.clearMap("eba:age");
        hazelcastService.clearMap("eba age");
        hazelcastService.clearMap("eba:state");
        hazelcastService.clearMap("eba state");
        hazelcastService.clearMap("ebaLocationIdentity");
        hazelcastService.clearMap("ebaInvolvedIdentity");
        hazelcastService.clearMap("ebaInvolvedOthersIdentity");

//        queueTest();
        hazelcastService.shutdown();
//        System.err.println("Client shutdown!");
    }

    public void queueTest() throws InterruptedException {
        BlockingQueue<String> queue = client.getQueue("queue");
        queue.put("Hello!");
        System.out.println("Message sent by Hazelcast Client!");

        System.err.println("clientName: "+client.getName());

        try {
            System.out.println(queue.take());
        } catch (HazelcastInstanceNotActiveException e) {
            System.err.println("Unable to take from the queue. Hazelcast Member is probably going down!");
        }
    }
}
