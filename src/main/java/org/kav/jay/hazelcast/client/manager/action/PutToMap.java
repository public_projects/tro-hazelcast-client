package org.kav.jay.hazelcast.client.manager.action;

import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PutToMap {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private HazelcastInstance client;

    public PutToMap(HazelcastInstance client){
        this.client = client;
    }

    public void put(String mapName, String message) {

    }
}
