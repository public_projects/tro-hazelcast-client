package org.kav.jay.hazelcast.client.config;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastConfig {

    @Bean
    public HazelcastInstance getClient(){
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress("127.0.0.1");
        clientConfig.getNearCacheConfigMap().keySet().forEach(s -> {
            System.err.println("cache config: "+ s);
        });
        return HazelcastClient.newHazelcastClient(clientConfig);
    }
}
